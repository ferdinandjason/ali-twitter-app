# Ali Twitter

## Description
As a user,
- I want to be able to create a tweet, so that I can express what is my mind.
- I want to be able delete a tweet, so that I can remove a tweet that I don't want to.
- I want to be able to see my tweets, so that I can read tweet I have created before.

This project enable CI and CD pipeline to make deployment faster.

## Table of Contents
- [Ali Twitter](#ali-twitter)
  - [Description](#description)
  - [Table of Contents](#table-of-contents)
  - [Environment Setup](#environment-setup)
    - [VirtualBox](#virtualbox)
    - [Vagrant](#vagrant)
    - [Ansible](#ansible)
  - [Environment Variables](#environment-variables)
  - [How to Use](#how-to-use)
    - [VM Architecture](#vm-architecture)
    - [Prepare](#prepare)
    - [CI/CD Pipeline](#cicd-pipeline)
    - [Open the Application](#open-the-application)

## Environment Setup
### VirtualBox
You can refer to this [VirtualBox Doc](https://www.virtualbox.org/wiki/Downloads) to download
the virtualbox and install it in your machine.

### Vagrant
You can refer to this [Vagrant Doc](https://www.vagrantup.com/intro/getting-started/install.html) to download
the vagrant and install it in your machine.

### Ansible
You can refer to this [Ansible Doc](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) to download the ansible and install it in your machine

## Environment Variables
Put neccessary environment variable in `.env`.
Please look `.env.example` as a template for your `.env`.

## How to Use
### VM Architecture
For more information about VM Architecture please visit [VM Architecture Docs](/docs/vm_infra.md)

### Prepare
1. After clone this projet, run this command to create VM based on above architecture.
   ```bash
    vagrant up
   ```
2. After the VM created, you can run this command to prepare the newly created VM.
   ```bash
    ansible-playbook --inventory=provision/hosts.yml provision/prepare_vm.yml
   ```
   If you want to know what application will be installed during this process please visit [Prepare VM Docs](docs/prepare_vm.md).
3. After that, you need extra step do prepare the GitLab Runner VM. Please visit [Extra Step on GitLab Runner VM Docs](docs/extra_gitlab_runner.md) and follow the instruction.


### CI/CD Pipeline
We use GitLab CI/CD Pipeline to test, build, and deploy the app. Please visit [CI/CD Pipeline Docs](docs/ci_cd.md) to see how it's work.

### Open the Application
You can open our Application by visit Load Balancer IP.
- Open `192.168.0.30/tweets` to see the Ali Twitter Application.
- Open `192.168.0.30:1936` to see HAProxy Statistic Report on ALi Twitter Application
