# Virtual Machine Architecture on Virtualization
These docs will explain about Virtual Machine Architecture on Virtualization.

## Table of Contents
- [Virtual Machine Architecture on Virtualization](#virtual-machine-architecture-on-virtualization)
  - [Table of Contents](#table-of-contents)
  - [Architecture Image](#architecture-image)
  - [Architecture](#architecture)
  - [VM Private Key](#vm-private-key)
    - [Placing the App VM's Private Key on GitLab Runner VM](#placing-the-app-vms-private-key-on-gitlab-runner-vm)
  - [Tips](#tips)


## Architecture Image
![IMG](img/VM&#32;Infrastructure&#32;DevOps&#32;Week&#32;One.png)

## Architecture
If you do `vagrant up` command it will make several VM with belom specification.

| Name | IP Address | OS | RAM | Description | Username |
| - | - | - | - | - | - |
| `db` | `192.168.0.33` | `Ubuntu 18.04` | `512 MB` | VM for database service `postgresql` | `vagrant` |
| `app1` | `192.168.0.34` | `Ubuntu 18.04` | `1024 MB` | First VM for application service | `vagrant` |
| `app2` | `192.168.0.35` | `Ubuntu 18.04` | `1024 MB` | Second VM for application service | `vagrant` |
| `lb` | `192.168.0.30` | `Ubuntu 18.04` | `512 MB` | VM for load balancer service | `vagrant` |
| `gitlab-runner` | `192.168.0.10` | `Ubuntu 18.04` | `1024 MB` | VM for GitLab Runner Service | `vagrant` |

On this project, there are two replicas of application service, and one replica of database and load balancer services.

You can check the VM status by run this command
```bash
vagrant status
```

## VM Private Key
These VM use private key as their login method. You can access the private key of each VM by run this command
```
cat .vagrant/machines/[VM Name]/virtualbox/private_key
```
with `[VM Name]` is Virtual Machine Name defined in [Architecture](#architecture-image) section.

You might copying this private key to your local machine `.ssh` folder or somewhere else. In the near future you need this private key, in order to run several command.

### Placing the App VM's Private Key on GitLab Runner VM
1. Copy the `app1`'s private key in the `.vagrant/machines/app1/virtualbox/private_key`
2. Paste to the GitLab Runner VM (do `vagrant ssh gitlab-runner` first) by run this command
   ```
   sudo su gitlab-runner
   mkdir -p ~/.ssh/app1
   vim ~/.ssh/app1/private_key
   ```
3. And paste the `app1`'s private key.
4. Copy the `app2`'s private key in the `.vagrant/machines/app2/virtualbox/private_key`
5. Paste to the GitLab Runner VM (do `vagrant ssh gitlab-runner` first) by run this command
   ```
   sudo su gitlab-runner
   mkdir -p ~/.ssh/app2
   vim ~/.ssh/app2/private_key
   ```
6. And paste the `app2`'s private key.

I use this method because it's simple, can be edited easily and doesn't require additional dependencies.If you have more app VM, you might considering to use `ssh-copy-id`.

## Tips
If you want to change the IP, you can do with `Replace All` feature in your most loved editor.
