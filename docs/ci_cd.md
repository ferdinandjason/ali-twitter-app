# CI/CD Pipeline

## Table of Contents
- [CI/CD Pipeline](#cicd-pipeline)
  - [Table of Contents](#table-of-contents)
  - [Diagram](#diagram)
  - [Pipeline](#pipeline)
    - [Test](#test)
    - [Build](#build)
    - [Deploy](#deploy)

## Diagram
![IMG](img/CI_CD&#32;Devops&#32;Wekk&#32;One.png)

## Pipeline
### Test
1. Set the Environtment Variables
2. Installing Gem by `bundle install`
3. Create the test database `bundle exec rake db:create RAILS_ENV=test`
4. Testing the app by `bundle exec rspec spec`

### Build
1. Yarn install by `yarn`
2. Precompiling asset by `bundle exec rails assets:precompile`
3. Delete unused code and asset by `rm -rf tmp/cache vendor/assets lib/assets spec`
4. Zip the code by `zip -r artifact_ali_twitter.zip . --exclude 'node_modules'`

### Deploy
1. Copy the artifact to ansible directory by `cp artifact_ali_twitter.zip provision/roles/deploy_app/files/artifact_ali_twitter.zip`
2. Deploy with Ansible Playbook by `ansible-playbook -i provision/hosts_gitlab.yml provision/deploy.yml`
