# Prepare VM

## Table of Contents
- [Prepare VM](#prepare-vm)
  - [Table of Contents](#table-of-contents)
  - [Application Installed on Each VM](#application-installed-on-each-vm)

## Application Installed on Each VM
1. VM `db` (Database Service)
   - PostgreSQL
2. VM `lb` (Load Balancer Service)
   - HAProxy
3. VM `app1` and `app2` (Application Service)
   - Ruby Language
   - git
   - zip
   - g++
   You can look other dependencies in [here](../../../provision/roles/install_dependencies/tasks/main.yml)
4. VM `gitlab-runner` (GitLab Runner Service)
   - Ruby Language
   - Node.js
   - Yarn
   - Ansible
   - git
   - zip
   - g++
   You can look other dependencies in [here](../../../provision/roles/install_dependencies/tasks/main.yml)
